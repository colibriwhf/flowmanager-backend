package css.unit.service.schema;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import css.services.flow.FlowService;
import css.services.schema.SchemaService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class SchemaServiceTests {
    private static ObjectMapper mapper = new ObjectMapper();

    private RestTemplate restTemplate;
    private SchemaListForTest schemas = new SchemaListForTest(Arrays.asList("flow1", "flow1_integer"));
    private SchemaService schemaService;
    private FlowService flowService;

    @BeforeEach
    public void setUp() throws JsonProcessingException {
        restTemplate = mock(RestTemplate.class);
        flowService = mock(FlowService.class);
        when(restTemplate.getForObject(anyString(), any()))
                .thenReturn(mapper.writeValueAsString(schemas));

        schemaService = new SchemaService(restTemplate , flowService, "not-used");
    }

    @Test
    public void get_all_schemas(){
        List<String> firstSchema = Arrays.asList("flow1");
        when(flowService.getAllSchemaInUse()).thenReturn(firstSchema);

        List<String> correctAvailableSchemas = Collections.singletonList("flow1_integer");

        List<String> allSchemaNames = schemaService.getAllAvailableSchema();
        assertEquals(allSchemaNames, correctAvailableSchemas);
    }


    class SchemaListForTest {
        public List<SchemaForTest> getEntities() {
            return entities;
        }

        public void setEntities(List<SchemaForTest> entities) {
            this.entities = entities;
        }

        private List<SchemaForTest> entities;

        public SchemaListForTest(List<String> entities) {
            this.entities = entities
                    .stream()
                    .map(x -> new SchemaForTest(x))
                    .collect(Collectors.toList());
        }
    }

    class SchemaForTest {
        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        private String name;

        public SchemaForTest(String name) {
            this.name = name;
        }
    }

}
