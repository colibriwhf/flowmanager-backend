package css.unit.service.flow;

import css.repositories.flow.Flow;
import css.repositories.flow.FlowRepository;
import css.services.flow.FlowService;
import css.utils.FlowForTest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

public class FlowServiceTests {

    private List<Flow> flows = new ArrayList<>();
    private FlowRepository flowRepository;
    private FlowService flowService;

    @BeforeEach
    public void setUp(){
        flowRepository = mock(FlowRepository.class);
        flowService = new FlowService(flowRepository);
        Flow flow = new FlowForTest("flow-pippo", "baudo");
        flows.add(flow);
        when(flowRepository.findAllByIsOldVersionFalse()).thenReturn(flows);
    }

    @Test
    public void get_all_latest_version_flows() {
        List<Flow> flowList = flowService.getAllLatestVersion();
        assertEquals(flowList, flows);
    }

    @Test
    public void add_new_flow_with_current_date() {
        Flow newFlow = new FlowForTest("flow-pippo", "baudo");
        doAnswer(invocation -> {
            Flow arg0 = invocation.getArgument(0, Flow.class);
            assertNotNull(arg0.getCreationDate());
            assertEquals(arg0.getCreationDate(), arg0.getLastUpdateDate());
            return null;
        }).when(flowRepository).save(any(Flow.class));

        flowService.save(newFlow);
    }

    @Test
    public void throws_exception_for_wrong_id_update() {
        when(flowRepository.findById(anyLong())).thenReturn(Optional.empty());
        assertThrows(NoSuchElementException.class,
                () -> flowService.update(0l, new Flow()));
    }

    @Test
    public void delete_existing_flow(){
        when(flowRepository.findById(anyLong())).thenReturn(Optional.of(new Flow()));

        doAnswer(invocation -> {
            Flow arg0 = invocation.getArgument(0, Flow.class);
            assertEquals(arg0.getIsOldVersion(), true);
            return null;
        }).when(flowRepository).save(any(Flow.class));

        flowService.delete(0l);
    }

    @Test
    public void delete_do_nothing_for_not_existing_flow(){
        when(flowRepository.findById(anyLong())).thenReturn(Optional.empty());

        flowService.delete(0l);
        verify(flowRepository, times(0)).save(any());
    }

    @Test
    public void update_status_change_only_status_without_creating_new_flow() {
        Flow flow = new FlowForTest("flow-pippo", "baudo");
        assertEquals(flow.getActive(), true);

        when(flowRepository.findById(0l)).thenReturn(Optional.of(flow));

        doAnswer(invocation -> {
            Flow arg0 = invocation.getArgument(0, Flow.class);
            assertEquals(arg0.getActive(), false);
            return null;
        }).when(flowRepository).save(any(Flow.class));

        flowService.updateStatus(0l, false);
        verify(flowRepository, times(1)).save(any());
    }

}
