package css.unit.controller.flow;

import com.fasterxml.jackson.databind.ObjectMapper;
import css.controllers.flow.FlowController;
import css.repositories.flow.Flow;
import css.services.flow.FlowService;
import css.utils.FlowForTest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(FlowController.class)
public class FlowControllerTests {
    private static ObjectMapper mapper = new ObjectMapper();
    private static String BASE_URL = "/flow";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private FlowService flowService;

    @Test
    public void get_all_accounts() throws Exception {

        List<Flow> flows = new ArrayList<>();
        flows.add(new FlowForTest("flow-name", "flow-owner"));

        // arrange
        when(flowService.getAllLatestVersion()).thenReturn(flows);
        // act and assert
        mockMvc.perform(get(BASE_URL)).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$..flowName").value("flow-name"))
                .andExpect(jsonPath("$..flowOwner").value("flow-owner"));

    }

    @Test
    public void get_status_ok_after_creation_new_flow() throws Exception {
        Flow flow = new FlowForTest("flow-name", "flow-owner");

        mockMvc.perform(
                post(BASE_URL)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(flow)))
                .andExpect(status().isOk());
    }

    @Test
    public void get_wrong_status_invoke_creation_with_wrong_flow() throws Exception {
        mockMvc.perform(
                post(BASE_URL)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("wrong json"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void delete_a_flow() throws Exception {
        mockMvc.perform(
                delete(BASE_URL + "/0"))
                .andExpect(status().isOk());
    }

}
