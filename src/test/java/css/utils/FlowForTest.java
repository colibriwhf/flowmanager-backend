package css.utils;

import css.repositories.flow.Flow;

import javax.validation.constraints.NotNull;

public class FlowForTest extends Flow {

    public FlowForTest(@NotNull String name, @NotNull String owner) {
        super();
        this.setFlowName(name);
        this.setFlowOwner(owner);
    }
}
