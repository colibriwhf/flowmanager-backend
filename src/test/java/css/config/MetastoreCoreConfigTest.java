package css.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;

import javax.sql.DataSource;

@Configuration
public class MetastoreCoreConfigTest {

    @Bean
    DataSource dataSource() {
        return new EmbeddedDatabaseBuilder()
                .setName("flows")
                .addScript("/testdb/test-schema.sql")
                .addScript("/testdb/test-data.sql")
                .build();
    }
}
