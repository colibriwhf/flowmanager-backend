package css.integration.flow;

import css.config.MetastoreCoreConfigTest;
import css.repositories.flow.Flow;
import css.services.flow.FlowService;
import css.utils.FlowForTest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.jdbc.Sql;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Import(MetastoreCoreConfigTest.class)
public class FlowControllerTests {

    private static String BASE_URL = "/flow";
    private static String ALL_FLOW_URL = BASE_URL + "/all";

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private FlowService flowService;

    @Test
    public void list_all_elements() {
        Flow[] results = restTemplate.getForObject(ALL_FLOW_URL, Flow[].class);

        assertEquals(results.length, 3);
    }

    @Test
    @Sql({"/testdb/test-schema.sql", "/testdb/test-data.sql"})
    public void list_all_latest_version_elements() {
        Flow[] results = restTemplate.getForObject(BASE_URL, Flow[].class);

        assertEquals(results.length, 2);
        assertEquals(results[0].getIsOldVersion(), false);
    }

    @Test
    @Sql({"/testdb/test-schema.sql", "/testdb/test-data.sql"})
    public void create_new_element() {
        Flow[] results = restTemplate.getForObject(BASE_URL, Flow[].class);
        assertEquals(results.length, 2);

        Flow newFlow = new FlowForTest("flow-twitter", "baudo");

        restTemplate.postForObject(BASE_URL, newFlow, Flow.class);

        Flow[] resultsWithNewElement = restTemplate.getForObject(BASE_URL, Flow[].class);
        assertEquals(resultsWithNewElement.length, 3);
        assertEquals(Arrays.stream(resultsWithNewElement)
                .filter(x -> x.getFlowName().equals("flow-twitter"))
                .count(), 1);
        assertTrue(Arrays.stream(resultsWithNewElement)
                .noneMatch(x -> x.getIsOldVersion()));
    }

    @Test
    @Sql({"/testdb/test-schema.sql", "/testdb/test-data.sql"})
    public void update_element() {

        Flow[] results = restTemplate.getForObject(ALL_FLOW_URL, Flow[].class);
        assertEquals(results.length, 3);

        results = restTemplate.getForObject(BASE_URL, Flow[].class);
        assertEquals(results.length, 2);
        assertEquals(results[0].getFlowOwner(), "gigi");
        assertEquals(results[1].getFlowOwner(), "alberto");

        String updateUrl = BASE_URL + "/0";
        Flow flowToUpdate = results[0];
        flowToUpdate.setFlowOwner("carrà");
        restTemplate.put(updateUrl, flowToUpdate);

        results = restTemplate.getForObject(ALL_FLOW_URL, Flow[].class);
        assertEquals(results.length, 4);

        results = restTemplate.getForObject(BASE_URL, Flow[].class);
        assertEquals(results.length, 2);
        assertEquals(Arrays.stream(results)
                .filter(x -> x.getFlowName().equals(flowToUpdate.getFlowName()))
                .collect(Collectors.toList())
                .get(0).getFlowOwner(), "carrà");
    }

    @Test
    @Sql({"/testdb/test-schema.sql", "/testdb/test-data.sql"})
    public void update_status_not_create_new_element() {

        Flow[] results = restTemplate.getForObject(ALL_FLOW_URL, Flow[].class);
        assertEquals(results.length, 3);

        results = restTemplate.getForObject(BASE_URL, Flow[].class);
        assertEquals(results.length, 2);
        assertEquals(results[0].getFlowOwner(), "gigi");
        assertEquals(results[1].getFlowOwner(), "alberto");

        assertEquals(Arrays.stream(results)
                .filter(x -> x.getFlowName().equals("flow-active"))
                .collect(Collectors.toList())
                .get(0).getActive(), true);

        String updateUrl = BASE_URL + "/0/false";
        restTemplate.put(updateUrl, null);

        results = restTemplate.getForObject(ALL_FLOW_URL, Flow[].class);
        assertEquals(results.length, 3);

        results = restTemplate.getForObject(BASE_URL, Flow[].class);
        assertEquals(results.length, 2);
        assertEquals(Arrays.stream(results)
                .filter(x -> x.getFlowName().equals("flow-active"))
                .collect(Collectors.toList())
                .get(0).getActive(), false);
    }

    @Test
    @Sql({"/testdb/test-schema.sql", "/testdb/test-data.sql"})
    public void delete_do_nothing_for_not_existing_flow() {

        Flow[] results = restTemplate.getForObject(ALL_FLOW_URL, Flow[].class);
        assertEquals(results.length, 3);

        String deleteUrl = BASE_URL + "/1511522";
        restTemplate.delete(deleteUrl);

        results = restTemplate.getForObject(ALL_FLOW_URL, Flow[].class);
        assertEquals(results.length, 3);
    }

    @Test
    @Sql({"/testdb/test-schema.sql", "/testdb/test-data.sql"})
    public void delete_an_existing_flow() {

        Flow[] results = restTemplate.getForObject(BASE_URL, Flow[].class);
        assertEquals(results.length, 2);

        String deleteUrl = BASE_URL + "/0";
        restTemplate.delete(deleteUrl);

        results = restTemplate.getForObject(BASE_URL, Flow[].class);
        assertEquals(results.length, 1);
        results = restTemplate.getForObject(ALL_FLOW_URL, Flow[].class);
        assertEquals(results.length, 3);
    }

    @Test
    @Sql({"/testdb/test-schema.sql", "/testdb/test-data.sql"})
    public void find_all_schema_in_use() {

        List<String> allSchemaInUse = flowService.getAllSchemaInUse();
        List<String> correctResult = Arrays.asList("schema-id1", "schema-id777");
        assertEquals(allSchemaInUse, correctResult);
    }
}
