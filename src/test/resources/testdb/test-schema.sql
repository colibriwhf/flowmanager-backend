drop table flow if exists;

CREATE TABLE flow
(
    id INTEGER identity primary key ,
    flow_name        varchar(255) NOT NULL,
    active           tinyint      DEFAULT NULL,
    creation_date    datetime(6)  DEFAULT NULL,
    file_format      varchar(255) DEFAULT NULL,
    flow_owner       varchar(255) NOT NULL,
    last_update_date datetime(6)  DEFAULT NULL,
    retention        int          DEFAULT NULL,
    schema_id        varchar(255) DEFAULT NULL,
    sensor_id        int          DEFAULT NULL,
    visibility       varchar(255) DEFAULT NULL,
    is_old_version   bit(1)       DEFAULT NULL
);
