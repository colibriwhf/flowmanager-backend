package css.controllers.flow;

import css.repositories.flow.Flow;
import css.services.flow.FlowService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;

@RestController()
@RequestMapping("/flow")
public class FlowController {
    private final Logger logger = LoggerFactory.getLogger(getClass());

    private FlowService flowService;

    public FlowController(FlowService flowService) {
        this.flowService = flowService;
    }

    @GetMapping()
    public List<Flow> getAllLatestVersion(){
        return flowService.getAllLatestVersion();
    }

    @GetMapping("/all")
    public List<Flow> getAll(){
        return flowService.getAll();
    }

    @PostMapping()
    @ResponseStatus(HttpStatus.OK)
    public void createIngestFlow(@RequestBody Flow newIngestFlow) {
        flowService.save(newIngestFlow);
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateFlow(@PathVariable Long id, @RequestBody Flow updatedFlow) {
        flowService.update(id, updatedFlow);
    }

    @PutMapping("/{id}/{status}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateStatus(@PathVariable Long id, @PathVariable Boolean status) {
        flowService.updateStatus(id, status);
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler({ NoSuchElementException.class })
    public void handleUnableToReallocate(Exception ex) {
        logger.error("Exception is: ", ex);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteFlow(@PathVariable Long id) {
        flowService.delete(id);
    }
}
