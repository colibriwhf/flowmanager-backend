package css.controllers.schema;

import css.services.schema.SchemaService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/schema")
public class SchemaController {

    private SchemaService schemaService;

    public SchemaController(SchemaService schemaService) {
        this.schemaService = schemaService;
    }

    @GetMapping
    public List<String> getAllAvailableSchemas() {
        return schemaService.getAllAvailableSchema();
    }

}
