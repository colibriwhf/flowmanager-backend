package css.repositories.flow;

public enum DataTypes {
    JSON,
    XML,
    CSV
}
