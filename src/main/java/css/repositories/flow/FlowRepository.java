package css.repositories.flow;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FlowRepository extends CrudRepository<Flow, Long> {

    List<Flow> findAllByIsOldVersionFalse();

    @Query("select f.schemaId from Flow f where f.isOldVersion = 0")
    List<String> findAllSchemaInUse();
}
