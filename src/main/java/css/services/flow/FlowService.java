package css.services.flow;

import css.repositories.flow.Flow;
import css.repositories.flow.FlowRepository;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;

@Service
public class FlowService {

    private FlowRepository flowRepository;

    public FlowService(FlowRepository flowRepository) {
        this.flowRepository = flowRepository;
    }

    public List<Flow> getAllLatestVersion() {
        return flowRepository.findAllByIsOldVersionFalse();
    }

    public List<String> getAllSchemaInUse() { return flowRepository.findAllSchemaInUse();}

    public List<Flow> getAll() {
        return (List<Flow>) flowRepository.findAll();
    }

    public void save(Flow newIngestFlow) {
        Date currentDate = new Date();
        newIngestFlow.setCreationDate(currentDate);
        newIngestFlow.setLastUpdateDate(currentDate);
        flowRepository.save(newIngestFlow);
    }

    @Transactional
    public void update(Long id, @NotNull Flow updatedFlow) throws NoSuchElementException {
        Flow oldFlow = flowRepository.findById(id).get();

        oldFlow.setLastUpdateDate(new Date());
        oldFlow.setIsOldVersion(true);

        flowRepository.save(oldFlow);
        updatedFlow.setId(null);
        this.save(updatedFlow);
    }

    @Transactional
    public void updateStatus(Long id, Boolean status) {
        Flow flow = flowRepository.findById(id).get();

        flow.setLastUpdateDate(new Date());
        flow.setActive(status);

        flowRepository.save(flow);
    }

    public void delete(Long id) {
        Flow flow = flowRepository.findById(id).orElse(null);

        if(flow == null)
            return;

        flow.setLastUpdateDate(new Date());
        flow.setIsOldVersion(true);
        flow.setActive(false);
        flowRepository.save(flow);
    }

}
