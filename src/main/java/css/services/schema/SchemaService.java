package css.services.schema;

import com.jayway.jsonpath.JsonPath;
import css.services.flow.FlowService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class SchemaService {

    private static String ALL_API = "/api/v1/schemaregistry/schemas/";

    private RestTemplate restTemplate;
    private String schemaUrl;
    private FlowService flowService;

    public SchemaService(RestTemplate restTemplate,
                         FlowService flowService,
                         @Value("${schema.registry.url}") String url) {
        this.restTemplate = restTemplate;
        this.flowService = flowService;
        this.schemaUrl = url;
    }

    public List<String> getAllAvailableSchema() {
        String response = restTemplate.getForObject(schemaUrl + ALL_API, String.class);
        List<String> allSchema = JsonPath.parse(response)
                .read("$..name");
        List<String> allSchemaInUse = flowService.getAllSchemaInUse();

        return allSchema
                .stream()
                .filter(x -> !allSchemaInUse.contains(x))
                .collect(Collectors.toList());
    }
}
