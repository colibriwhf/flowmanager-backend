create database ingest_metastore DEFAULT CHARACTER SET utf8mb3;

create user 'ingest_admin'@'%' identified by '123Stella!';
grant all on ingest_metastore.* to 'ingest_admin'@'%';

--FOR TESTING
create database ingest_metastore_test DEFAULT CHARACTER SET utf8mb3;
--IF NOT EXISTS--
create user 'ingest_admin'@'%' identified by '123Stella!';
--IF NOT EXISTS--
grant all on ingest_metastore_test.* to 'ingest_admin'@'%';
