# README #

Il backend è realizzato con teconologia Spring Boot. Il corretto funzionamento si basa sulla presenza di un database msyql sottostante (vedi file resources/application.properties per i puntamenti attualmente configurati).
Di seguito le istruzioni per la creazione del database:

	create database ingest_metastore DEFAULT CHARACTER SET utf8mb3;
	create user 'ingest_admin'@'%' identified by '*******';
	grant all on ingest_metastore.* to 'ingest_admin'@'%';

